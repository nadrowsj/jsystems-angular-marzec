# GIT 
git clone https://bitbucket.org/ev45ive/jsystems-angular-marzec.git jsystems-angular-marzec
cd jsystems-angular-marzec
<!-- code "." -->
npm install
npm start



git stash -u
git stash -u -m "opis zmian"
<!-- git pull -t origin master -->
git pull 

## Semver
<!-- Exact form package-lock -->
<!-- npm ci -->
https://semver.npmjs.com/
https://semver.org/

# Instalacje 

node LTS
node -v
v16.13.1

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.65.1

Google Chrome	chrome://version/
99.0.4844.51

# CLI
https://angular.io/cli

npm i -g @angular/cli 

ng --version
Angular CLI: 13.2.6
Node: 16.13.1
Package Manager: npm 6.14.6
 
ng help
ng new --help

## Nowy projekt
ng new nazwa-aplikacji

## Dev Tools
https://angular.io/guide/devtools

https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh

## UI Toolkits
https://material.angular.io/components/categories
https://ng-bootstrap.github.io/#/home
https://www.primefaces.org/primeng/#/
https://ng.ant.design/docs/introduce/en
https://www.telerik.com/kendo-angular-ui
https://www.ag-grid.com/angular-data-grid/

## Angular Language Service
https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2

https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode


## NgCLI schematics ( code generator )
<!-- ng generate module -->

ng g m shared -m app 
ng g c shared/components/clock -s -t --export 
ng g c shared/components/nav-bar --export


## Playlists Module


ng g m playlists --routing --route playlists -m app

ng g c playlists/containers/playlists --type container --export

ng g c playlists/components/playlists-list
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-editor


## Validators
ng g d shared/validators/censor --export


## Music Module

ng g m music --routing --route music -m app

ng g c music/containers/album-search --type container --export
ng g c music/containers/album-details --type container --export

ng g c music/components/search-form
ng g c music/components/results-grid
ng g c music/components/album-card

# Core module + Music API

ng g m core -m app
ng g s core/services/music-api --flat false
ng g i core/model/search

## Quicktype
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## NgModel
selector: 'input[ngModel]'

ngModel [ngModel]="zmienna" (ngModelChange)="zmienana = $event"

[(ngModel)]="zmienna"
[PLacki] (PLackiChange)  [(PLacki)]
[ABC] (ABCChange) [(ABC)]

[ngModel]=zmienna
 <!-- (ngModelChange)='zmienna = $event' -- nie zadziala-->

ngModel (ngModelChange)='zmienna = $event'

### Prettier AutoFormatter
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

https://prettier.io/docs/en/integrating-with-linters.html

## Oauth
    
https://github.com/manfredsteyer/angular-oauth2-oidc
npm i angular-oauth2-oidc --save

ng g s core/services/auth 

## RxJS operators

https://rxmarbles.com/#pluck

https://rxjs.dev/operator-decision-tree
https://rxjs.dev/api/operators/map
https://rxviz.com/

