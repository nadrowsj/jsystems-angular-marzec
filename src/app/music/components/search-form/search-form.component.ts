import { FormControl, FormGroup, NgForm, Validator, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { debounceTime, distinctUntilChanged, filter, map, merge, Subject, Subscription, throttleTime } from 'rxjs';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {

  queryForm = new FormGroup({
    'query': new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    'type': new FormControl('album'),
  })
  queryField = this.queryForm.get('query') as FormControl

  @Input() set query(q: string | null) {
    this.queryField.setValue(q, {
      emitEvent: false // Do NOT emit valueChanges
    })
  }

  @Output() search = new EventEmitter<string>();

  subscription = new Subscription()

  submitClicks = new Subject<null>();

  buttonQueries = this.submitClicks.pipe(
    map(() => this.queryField.value),
    throttleTime(1500)
  )

  typeaheadQueries = this.queryField.valueChanges.pipe(
    debounceTime(400),
    filter(q => q.length >= 3),
    distinctUntilChanged(),
  )

  constructor() { }

  submitSearch() {
    if (this.queryForm.invalid) return;
    this.submitClicks.next(null)
  }

  ngOnInit(): void {
    this.subscription.add(
      merge(this.buttonQueries, this.typeaheadQueries)
        .subscribe(this.search)
    )
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

}
