import { AbstractControl, FormControl, FormGroup, NgForm } from '@angular/forms';
import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-form-messages',
  templateUrl: './form-messages.component.html',
  styleUrls: ['./form-messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormMessagesComponent implements OnInit {

  @Input() field!: AbstractControl | null

  // formRef?: NgForm

  constructor(
    // public formRef: NgForm
  ) { }

  ngOnInit(): void {
    // this.formRef = this.field?.parent as FormGroup
  }

}
