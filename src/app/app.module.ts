import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { INITIAL_RESULTS } from './core/tokens';
import { mockAlbums } from './core/mocks/mockAlbums';
import { MusicModule } from './music/music.module';
import { MockMusicAPIService } from './core/mocks/music-api.service';
import { MusicAPIService } from './core/services/music-api/music-api.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    // MocksModule,
  ],
  providers: [
    {
      provide: INITIAL_RESULTS,
      useValue: mockAlbums
    },
    // {
    //   provide: MusicAPIService,
    //   useClass: MockMusicAPIService
    // }
  ],
  bootstrap: [AppComponent /* HeaderComponent, SidebarComponent */]
})
export class AppModule { }

// export class AppModule implements DoBootstrap {
//   ngDoBootstrap(appRef: ApplicationRef): void {
//     appRef.bootstrap(AppComponent, 'app-root')
//   }
// }
