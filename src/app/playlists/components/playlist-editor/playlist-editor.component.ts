import { NgIf } from '@angular/common';
import { Playlist } from './../../containers/playlists/playlists.container';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';

NgIf
@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit, OnChanges {

  @Input() playlist = { id: '', name: '', public: false, description: '' }

  @Output() cancel = new EventEmitter<Playlist['id']>();
  @Output() save = new EventEmitter<Playlist>();

  cancelClicked() {
    if (this.playlist) {
      this.cancel.emit(this.playlist.id)
    }
  }

  submit(formRef: NgForm) {
    if (formRef.invalid) {
      return
    }
    formRef.control.disable()

    this.save.emit({
      id: this.playlist?.id,
      ...formRef.value
    })
  }

  ngOnChanges(changes: SimpleChanges): void { }

  constructor() {
  }

  ngOnInit(): void {
  }

}
