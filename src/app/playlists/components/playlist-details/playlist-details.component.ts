import { Playlist } from './../../containers/playlists/playlists.container';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist?: Playlist;


  @Output() edit = new EventEmitter();

  editClicked() {
    this.edit.emit()
  }

  @Output() cancel = new EventEmitter();

  cancelClicked() {
    this.cancel.emit()
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
