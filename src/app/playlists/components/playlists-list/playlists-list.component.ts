import { NgForOf, NgForOfContext, NgIf, NgIfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../containers/playlists/playlists.container';

NgIf
NgIfContext
NgForOf
NgForOfContext
@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistsListComponent implements OnInit {

  @Input('items') playlists: Playlist[] = []

  @Input() selectedId? = ''
  @Output() selectedIdChange = new EventEmitter<Playlist['id']>();
  @Output() remove = new EventEmitter<Playlist['id']>();

  constructor() { }

  select(id: string) {
    this.selectedIdChange.emit(id)
  }
  removeClick(id: string) {
    this.remove.emit(id)
  }

  ngOnInit(): void {
  }

  myTrackFn(index: number, item: any) {
    return item.id
  }
}
