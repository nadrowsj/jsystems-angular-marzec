import { Component, OnInit } from '@angular/core';

declare global {
  interface Crypto {
    randomUUID: () => string;
  }
}

type Modes = 'details' | 'editor' | 'create';

export interface Playlist {
  id: string;
  name: string;
  public: boolean;
  description: string;
}

@Component({
  selector: 'app-playlists-container',
  templateUrl: './playlists.container.html',
  styleUrls: ['./playlists.container.scss']
})
export class PlaylistsContainer implements OnInit {

  mode: Modes = 'details'


  playlists: Playlist[] = [
    {
      id: '123',
      name: 'Playlist 123',
      public: false,
      description: 'the best playlist'
    }, {
      id: '234',
      name: 'Playlist 234',
      public: true,
      description: 'Best playlist ever'
    }, {
      id: '345',
      name: 'Playlist 345',
      public: false,
      description: 'Best playlist'
    }
  ]

  selectedId?: Playlist['id']
  selected: Playlist | undefined;

  selectPlaylist(id?: Playlist['id']) {
    this.selectedId = id;
    this.selected = id ? this.playlists.find(p => p.id === id) : undefined
  }

  constructor() { }

  showDetails() {
    this.mode = 'details'
  }

  showEditor() {
    this.mode = 'editor'
  }

  showCreate() {
    this.mode = 'create'
  }

  removePlaylist(id: Playlist['id']) {
    this.playlists = this.playlists.filter(p => p.id === id)

    if (id === this.selectedId) {
      this.selectPlaylist()
    }
  }

  addPlaylist(draft: Playlist) {
    draft.id = crypto.randomUUID()
    this.playlists = [...this.playlists, draft]

    this.selectPlaylist(draft.id)
    this.showDetails()
  }

  savePlaylist(draft: Playlist) {
    setTimeout(() => {

      this.playlists = this.playlists.map(p => p.id === draft.id ? draft : p)

      this.selectPlaylist(draft.id)
      this.showDetails()
    }, 2000)
  }

  ngOnInit(): void {
  }

}
