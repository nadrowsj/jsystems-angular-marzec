import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'music', pathMatch: 'full'
  },
  {
    path: 'playlists',
    loadChildren: () => import('./playlists/playlists.module')
      .then(m => m.PlaylistsModule)
  },
  {
    path: 'music',
    loadChildren: () => import('./music/music.module')
      .then(m => m.MusicModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // useHash: true // history.pushState - default
    // anchorScrolling:true,
    // scrollPositionRestoration:'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
