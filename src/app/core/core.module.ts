import { Album } from './model/search';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HttpHandler, HttpXhrBackend, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { API_URL, INITIAL_RESULTS } from './tokens';
import { MusicAPIService } from './services/music-api/music-api.service';
import { AuthConfig, OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptor } from './services/auth/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    },
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    {
      provide: INITIAL_RESULTS,
      useValue: []
    },
    // {
    //   provide: MusicAPIService,
    //   useFactory(api: string, results: Album[], http: HttpClient) {
    //     return new MusicAPIService(api, results, http)
    //   },
    //   deps: [API_URL, INITIAL_RESULTS, HttpClient]
    // }
    // { provide: HttpClient, useClass: HttpClient }
    // HttpClient,
    // { provide: HttpHandler, useClass: HttpXhrBackend }

    // Override:
    // { provide: HttpHandler, useClass: MySuperAwesomeHttpXhrBackend }
  ]
})
export class CoreModule {
  // private auth: AuthService;

  constructor(private auth: AuthService) {
    // this.auth = auth

    this.auth.init()
  }

}
