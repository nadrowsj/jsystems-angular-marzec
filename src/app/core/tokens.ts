import { InjectionToken } from '@angular/core';
import { Album } from './model/search';

export const API_URL = new InjectionToken<string>('URL For API');
export const INITIAL_RESULTS = new InjectionToken<Album[]>('Initial results');
