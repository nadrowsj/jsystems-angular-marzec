import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private oAuth: OAuthService
  ) {
    // this.oAuth.configure()
  }

  init() {
    from(this.oAuth.tryLogin())
      .subscribe(() => {
        // autologin
        if (!this.getToken()) {
          this.login()
        }
      })
  }

  login() {
    this.oAuth.initLoginFlow()
  }

  logout() {
    this.oAuth.logOut()
  }

  getToken() {
    return this.oAuth.getAccessToken()
  }

}
